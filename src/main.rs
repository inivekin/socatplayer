/* socatplayer
 *
 * A simple utility to playback socat logs
 *
 * Parses a socat log (e.g. a socat log left by a command like
 * `socat -x OPEN:/dev/ttyS0,b115200,echo=0,icanon=0 \
 * PTY,link=/dev/ttyO0,rawer 2> socat_log.log`)
 * and replays the contents to stdout or the specified files.
 *
 * example use:
 *
 * replay logged incoming traffic only:
 * cat socat_log.log | socatplayer -o /dev/null > incoming_only.hex
 *
 * replay all incoming/outgoing logged data to stdout:
 * socatplayer -l socat_log.log > all_data.hex
 *
 * replay only outgoing logged data ignoring timing data:
 * cat socat_log.log | socatplayer -i /dev/null --immediate > outgoing_only.hex
 *
 * @author: <inivekin@gmail.com>
 * */
use std::fs::OpenOptions;
use std::io;
use std::io::{BufRead, BufReader, BufWriter, Write};

use chrono::{Duration, NaiveDate, NaiveDateTime};
use clap::Clap;
use regex::Regex;

#[derive(Clap)]
#[clap(version = "0.1", author = "inivekin <inivekin@gmail.com>")]
struct Opts {
    #[clap(short, long)]
    logfile: Option<String>,

    #[clap(short, long)]
    incoming_out: Option<String>,
    #[clap(short, long)]
    outgoing_out: Option<String>,

    #[clap(long)]
    immediate: bool,

    #[clap(short, long)]
    hexdump: bool
}

enum SocatInOut<'t> {
    In(Option<regex::Captures<'t>>),
    Out(Option<regex::Captures<'t>>),
}

fn main() -> Result<(), std::io::Error> {
    let opts: Opts = Opts::parse();
    let stdin;
    let mut stdin_lines;
    let mut in_file_lines;

    // get reader for log file as dynamic iterator trait object
    let reader_stream: &mut dyn Iterator<Item = _> = match opts.logfile {
        Some(log_filename) => {
            let file = OpenOptions::new().read(true).open(log_filename)?;
            in_file_lines = BufReader::new(file).lines();
            &mut in_file_lines
        }
        None => {
            stdin = io::stdin();
            stdin_lines = stdin.lock().lines();
            &mut stdin_lines
        }
    };

    // FIXME(kevinc) There is a better way to handle stdout here...
    // set up writers for outputting the data
    let stdout = io::stdout();
    let mut stdout_writer = BufWriter::new(Box::new(stdout) as Box<dyn Write>);
    let incoming_writer_file: BufWriter<Box<dyn Write>>;
    let outgoing_writer_file: BufWriter<Box<dyn Write>>;
    let mut incoming_writer: Option<BufWriter<_>> = if let Some(filename) = opts.incoming_out {
        let file = OpenOptions::new().create(true).write(true).open(filename)?;
        incoming_writer_file = BufWriter::new(Box::new(file) as Box<dyn Write>);
        Some(incoming_writer_file)
    } else {
        None
    };
    let mut outgoing_writer: Option<BufWriter<_>> = if let Some(filename) = opts.outgoing_out {
        let file = OpenOptions::new().create(true).write(true).open(filename)?;
        outgoing_writer_file = BufWriter::new(Box::new(file) as Box<dyn Write>);
        Some(outgoing_writer_file)
    } else {
        None
    };

    let mut write_to_writer_or_stdout = |data: &[u8], writer: &mut Option<BufWriter<Box<dyn Write>>>| {
        if let Some(writer) = writer {
            writer
                .write_all(data)
                .expect("Could not write incoming data to incoming data file.");
            writer
                .flush()
                .expect("Could not flush incoming data from buffer for incoming data file.");
        } else {
            stdout_writer
                .write_all(data)
                .expect("Could not write incoming data to stdout.");
            stdout_writer
                .flush()
                .expect("Could not flush incoming data from buffer for stdout.");
        }
    };
    // set up regular expressions for parsing the socat logs
    let data_in_out_re = Regex::new(r"(?x)^[<>]\s
                                 (?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d{2})\s
                                 (?P<hour>\d{2}):(?P<minute>\d{2}):(?P<second>\d{2}).(?P<microsec>\d{6})\s\s
                                 length=\d*\sfrom=\d*\sto=\d*$").unwrap();
    // NOTE(kevinc) intentially discard rest of line in case of -h flag
    let data_re = Regex::new(r"^(\s[0-9A-Fa-f]{2})+").unwrap();
    let _data_end = Regex::new(r"^\-\-$").unwrap();

    // simple dodgy method to store the time of the last data block for difference calculation
    // FIXME(kevinc) would be cleaner to use peekable
    let mut last_time: Option<NaiveDateTime> = None;

    // the parsing loop
    while let Some(Ok(start_line)) = &reader_stream.take(1).collect::<Vec<_>>().get(0) {
        let data_block_start = match start_line.chars().nth(0) {
            Some('>') => Ok(SocatInOut::In(data_in_out_re.captures(&start_line))),
            Some('<') => Ok(SocatInOut::Out(data_in_out_re.captures(&start_line))),
            _ => {
                eprintln!("Invalid start line {:?}", &start_line);
                Err(&start_line)
            }
        };

        if !opts.immediate {
            let time_diff = match data_block_start {
                Ok(SocatInOut::In(Some(ref d))) | Ok(SocatInOut::Out(Some(ref d))) => {
                    // FIXME(kevinc) self-explanatory...
                    let (year, month, day) = (
                        i32::from_str_radix(d.name("year").unwrap().as_str(), 10).unwrap(),
                        u32::from_str_radix(d.name("month").unwrap().as_str(), 10).unwrap(),
                        u32::from_str_radix(d.name("day").unwrap().as_str(), 10).unwrap(),
                    );
                    let (hour, minute, second, nanos) = (
                        u32::from_str_radix(d.name("hour").unwrap().as_str(), 10).unwrap(),
                        u32::from_str_radix(d.name("minute").unwrap().as_str(), 10).unwrap(),
                        u32::from_str_radix(d.name("second").unwrap().as_str(), 10).unwrap(),
                        u32::from_str_radix(d.name("microsec").unwrap().as_str(), 10).unwrap()
                            * 1000,
                    );
                    let current_time = NaiveDate::from_ymd(year, month, day)
                        .and_hms_nano(hour, minute, second, nanos);
                    let time_diff = if let Some(prev) = last_time {
                        (current_time - prev) as Duration
                    } else {
                        Duration::zero()
                    };
                    last_time = Some(current_time);
                    time_diff
                }
                _ => {
                    eprintln!("Due to invalid data start line, no time is waited before transmitting this sequence...");
                    last_time = None;
                    Duration::zero()
                }
            };

            std::thread::sleep(
                time_diff
                    .to_std()
                    .expect("Failed to process time interval - cannot time travel"),
            );
        }

        // parses the string containing the hex byte sequence (easier than including in the reg expression)
        let parse_data_line = |line: &str| {
            data_re.captures(line).unwrap()[0]
                .trim()
                .split(' ')
                .map(|hex| u8::from_str_radix(hex, 16))
                .filter_map(|hex| hex.ok())
                .collect::<Vec<_>>()
        };

        let mut push_data_line_to_writer = |line: &str| {
            let data = parse_data_line(&line);
            match data_block_start {
                Ok(SocatInOut::In(_)) => write_to_writer_or_stdout(&data, &mut incoming_writer),
                Ok(SocatInOut::Out(_)) => write_to_writer_or_stdout(&data, &mut outgoing_writer),
                _ => eprintln!("Do not know where to write {:?}", &data)

            };
        };

        // push data to the relevant writers
        if opts.hexdump {
            reader_stream
                .filter_map(|line| line.ok())
                .take_while(|line| line.starts_with(' '))
                .for_each(|line| push_data_line_to_writer(&line));
        } else {
            reader_stream
                .filter_map(|line| line.ok())
                .take(1)
                .for_each(|line| push_data_line_to_writer(&line));

        }
    }

    Ok(())
}
